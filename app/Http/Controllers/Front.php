<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;


class Front extends Controller
{
    var $categories;

    public function __construct() {
        $this->categories = Category::all(array('name'));
      }

    public function blog() {
        $posts = Post::where('id', '>', 0)->paginate(3);
        $posts->setPath('blog');

        $data['posts'] = $posts;

        return view('blog', array('data' => $data, 'title' => 'Latest Blog Posts', 'page' => 'blog', 'description' => '', 'categories' => $this->categories));
    }
      
      public function blog_post($url) {
        $post = Post::where('url', '=' , $url)->first();
    
            $previous_url = Post::prevBlogPostUrl($post->id);
            $next_url = Post::nextBlogPostUrl($post->id);
    
            $data['tags'] = $post->tags;
            $data['title'] = $post->title;
            $data['description'] = $post->description;
            $data['content'] = $post->content;
            $data['blog'] = $post->blog;
            $data['created_at'] = $post->created_at;
            $data['image'] = $post->image;
            $data['previous_url'] = $previous_url;
            $data['next_url'] = $next_url;
    
    
        return view('blog_post', array('data' => $data, 'page' => 'blog'));
      }
}
?>